# Keycash Frontend Code Challenge

O desafio consiste em construir uma vitrine de imóveis para keycash, onde o usuário poderá visualizar uma lista dos imóveis disponíveis, filtrar sua busca por critrérios pré-definidos e clicar nos cards dos imóveis para saber mais detalhes sobre o mesmo.

## O que vamos avaliar?
Para resolver este problema sugerimos a utilização de um dos frameworks: 

* Para web Angular, React.js ou Vue.js
* Mobile Ionic (Angular, React.js, Vue.js ou Javascript) ou React Native

Tente implementar o máximo que conseguir do teste no tempo determinado, se não conseguir implementar todas as features sugeridas, fique tranquilo, avaliaremos sua solução pelo que foi entregue :)

Iremos avaliar a sua solução pela:

* Organização
* Manutenibilidade
* Rastreabilidade
* Portabilidade
* Responsividade das telas (Trabalhamos muito com a visão de mobile-first) 
* Entendimento do problema (sua interpretação, bem como seu entendimento a respeito das regras propostas também faz parte do teste, pois estamos avaliando a forma como você entende e resolve problemas)

Esperamos um projeto **PRODUCTION READY**, logo utlize de todas as boas práticas que está habituado para implementar seu código.

## O que precisa ser implementado?

### Telas

##### Tela de vitrine de imóveis;
   1. Listagem dos imóveis em cards;
   2. Filtros para busca na lista (preço, endereço, área útil, vagas, banheiros e dormitórios);
   3. Paginar a lista em 5, 10 e 15 items por página;
   
##### Tela de detalhes do imóvel;
   1. Mostrar as imagens do imóvel;
   2. Mostrar detalhes do imóvel;

**obs:** Use sua criatividade para mostrar todos os detalhes do imóvel na tela.

### Regras de negócio

1. Um imóvel só poderá ser mostrado se o campo publish for true;
2. Imóveis sem localização não deverão ser mostrados na lista;
3. A lista de imóveis deve começar a ser exibida do maior para o menor preço;

### Chamada da API

`GET http://5e148887bce1d10014baea80.mockapi.io/keycash/challenge`

### Objeto de retorno da API
A API retorna um array de imóveis, como no exemplo:

```json
[
  ...
  {
    "id": "b3bb6f60-a6c8-467f-a23c-b5dea21b038c",
    "address": {
      "formattedAddress": "Rua Tabapuã, 56 Itaim Bibi - SP",
      "geolocation": {
        "lat": -23.581850,
        "lng": -46.670970
      }
    },
    "images": [
      "https://i.imgur.com/q1Mj5mf.jpg",
      "https://i.imgur.com/SDq8t69.jpg",
      "https://i.imgur.com/QHy2i57.jpg",
      "https://i.imgur.com/VbCUqNc.jpg",
      "https://i.imgur.com/AmcZKVA.jpg",
      "https://i.imgur.com/q1Mj5mf.jpg",
      "https://i.imgur.com/6m0RetO.jpg"
    ],
    "price": 1180000,
    "bathrooms": 2,
    "bedrooms": 2,
    "parkingSpaces": 2,
    "usableArea": 96,
    "publish": true
  }
]
   
```

### Dicas para o código

* Separe bem a lógica de seus componentes, isso ajuda na manutenibilidade;
* Fique livre para utilizar bibliotecas que possam facilitar seu trabalho; 

### Bonus

* Hoje nossa stack frontend é toda em vue.js, logo será um a mais se você utilizar esse framework;
* Utlizar flux para armazenar os imóveis;


## Como entregar

Para entregá-lo crie um repositório no GitHub ou Bitbcuket. Para facilitar a identificação do seu desafio, nomeie seu repositório para keycash-frontend-challenge-{seu-nome} (ex: keycash-frontend-john-doe).

Pedimos que você faça um README com pelo menos instruções básicas como:

* Como rodar localmente?
* Como fazer o deploy?
* Apenas se for mobile. Como gerar o APK?

Bom teste!
